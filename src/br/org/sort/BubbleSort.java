package br.org.sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class BubbleSort{

	public static void main(String args[]) {
	
		Scanner scn = new Scanner(System.in);

		Random rdn = new Random();
		
		List<String> resultados = new ArrayList<String>();

		boolean desliga = false;

		while (!desliga) {
			
			int tamanho = 0;
			System.out.println("\nDigite o tamanho do vetor:");
			
			tamanho = Integer.parseInt(scn.nextLine());
			
			int vetor[]  = new int[tamanho];
			
			String numeros = "";
			System.out.println("Digite a sequ�ncia separada por v�rgula (,) ou * para random");

			numeros = scn.nextLine();
			
			if (numeros.trim().equals("*")) {

				for (int i = 0; i < tamanho; i++) {

					int rando = (rdn.nextInt() / 1000)
							* -3
							+ Integer.parseInt(String.valueOf(Math.E)
									.replace(".", "").substring(0, 4));

					
					vetor[i] = rando;
					
				}
				
			} else {

				String[] aux;

				aux = numeros.split(",");

				int tmp_int = 0;

				for (int i = 0; i < tamanho; i++) {
					
					tmp_int = Integer.parseInt(aux[i]);
					vetor[i] = tmp_int;
				}

			}
			
			int bubble = bubbleSort( vetor);
			int quick = quickSort( vetor,  0,  tamanho-1);

			System.out.printf("\nNum. de operacoes bubble: %d ", bubble);
			
			System.out.printf("\nNum. de operacoes quick: %d ",quick );
			
			resultados.add("\nNum. de operacoes bubble: "+ bubble+"\nNum. de operacoes quick: "+ quick+ "\ntam do vet: "+tamanho);
			
			System.out.println("\n1 - Continua \n2 - Para");
			desliga = scn.nextLine() == "2" ? true : false;
			
			int count = resultados.size();
			
			for(String res : resultados)
				System.out.println("\n resultado ("+count--+") :" + res);
			
	
		}
		
	}

	// BubbleSort
	public static int bubbleSort(int[] vetor) {

		int op = 0;  // contador de operacoes
		
		boolean houveTroca = true;
		op++;

		while (houveTroca) {
			houveTroca = false;
			op++;
			for (int i = 0; i < (vetor.length) - 1; i++) {
				
				if (vetor[i] > vetor[i + 1]) {
					int variavelAuxiliar = vetor[i + 1];    
					op++;
					vetor[i + 1] = vetor[i];				
					op++;
					vetor[i] = variavelAuxiliar;			
					op++;
					houveTroca = true;						
					op++;
				}
				op++;
			}
			op++;
		}
		op++;
		
		return op;
	}

	// QuickSort
	public static int quickSort(int[] v, int ini, int fim) {
		
		int op = 0;
		int meio;

		if (ini < fim) {
			//System.out.printf("ini: %d \nfim: %d ",ini, fim);
			meio = partition(v, ini, fim)[0];
			op++;
			quickSort(v, ini, meio);
			quickSort(v, meio + 1, fim);
		}
		op++;
		
		op += partition(v, ini, fim)[1];
		
		return op;
	}

	public static int[] partition(int[] v, int ini, int fim) {
		int pivo, topo, i;
		
		int op = 0;
		
		pivo = v[ini];
		op++;
		
		topo = ini;
		op++;

		for (i = ini + 1; i <= fim; i++) {
			if (v[i] < pivo) {
				v[topo] = v[i];
				op++;
				
				v[i] = v[topo + 1];
				op++;
				
				topo++;
				op++;
			}
			op++;
		}
		op++;
		
		v[topo] = pivo;
		op++;
		
		int aux[] = new int[2];
		aux[0] = topo;
		aux[1] = op;
		return aux;
	}

	

}
